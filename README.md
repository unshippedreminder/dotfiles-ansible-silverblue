# Personal Dotfiles for Fedora Silverblue

## How to run

### Install

Run `sudo curl https://gitlab.com/unshippedreminder/dotfiles-ansible-silverblue/-/raw/master/bootstrap.py | sudo python`

Note:
If you just installed Silverblue, you may have to wait until your system has run the first update. See [#4](https://gitlab.com/unshippedreminder/dotfiles-ansible-silverblue/-/issues/4)

### Setup

#### Dconf

This playbook can't manipulate dconf (i.e. GNOME settings and extension settings) due to #30.
This means setting these manually, from scratch or a backup.

#### Backup (Borgmatic)

Borgmatic is automatically installed and configured.

If you need to initialize a new borg repository, use this command `borg init -e repokey-blake2 root@borgbackup.lan:/root/borgbackup/mars`

The `borg_repo` is hardcoded in `local.yml` and an SSH keypair is generated for `root` who is doing the backing up.

You need to run `ssh-copy-id root@borgbackup.lan` (or whatever borg repository you use) as root to be able to run the automatic backup.

Borgmatic will look for the repokey in root's `$HOME/.borg-passphrase`

## Spec

### Must Have

- [x] Must *succesfully run, even on first boot*.
- [ ] Must be able to install packages with:
  - [x] RPM-OSTree
  - [x] Flatpak
  - [ ] Toolbox Containers

### Nice to Haves

- [x] Should be able to *execute across reboot*.
- [ ] Install VSCode extensions.
- [ ] Install GNOME Extensions
- [ ] Setup Mozilla Sync

### Bootstrap Steps

1. install ansible
2. restart
3. run playbook

#### How to: Survive Reboot

Create a shellscript that creates a SystemD service that runs at boot and deletes itself.

1. Pull service file from Git.
2. Place it in `/etc/systemd/system/`.
3. Enable the service.
4. Reboot.
5. Service will run on boot.
6. Disable the service file

## Dev Environment

- Fedora Silverblue 31/32
  1. Autologin = true
  2. Screen Blanking = false
  3. `rpm-ostree install spice-webdavd`
  4. Share this directory with the dev VM.
  5. Snapshot "Ready Pre Update"
     - This snapshot is used to troubleshoot [#4](https://gitlab.com/unshippedreminder/dotfiles-ansible-silverblue/-/issues/4)
  6. `rpm-ostree upgrade`
  7. Snapshot "Ready Post Update"

## Related projects

This project relies on these repositories:
- [OCI Image for running RCM on Silverblue with Podman](https://gitlab.com/unshippedreminder/oci-utility-rcm)
- [Personal Dotfiles Repository](https://gitlab.com/unshippedreminder/dotfiles)
- [Personal Toolbox Image](https://gitlab.com/unshippedreminder/fedora-toolbox-custom)
