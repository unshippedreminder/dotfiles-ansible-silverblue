#!/usr/bin/env python3
import getpass
import subprocess
import urllib.request
import time

repository="https://gitlab.com/unshippedreminder/dotfiles-ansible-silverblue/"
branch="master"
service_name="dotfiles-persist-reboot.service"
# service_url: A direct link to raw .service file in Git repo.
service_url=repository + "-/raw/" + branch + "/files/"  + service_name
# service_dir: Directory to install .service files to
service_dir="/etc/systemd/system/"
service_file=service_dir + service_name
hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
       'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
       'Accept-Encoding': 'none',
       'Accept-Language': 'en-US,en;q=0.8',
       'Connection': 'keep-alive'}

class bcolors:
    OKGREEN = '\033[92m'
    WARNING = '\033[91m'
    ENDC = '\033[0m'

def writetofile(path, text):
    try:
        file = open(path, "x")
        file.write(text)
        file.close()
        pass
    except FileExistsError:
        file = open(path, "w")
        file.write(text)
        file.close()
        pass

def rpmostree_install(application):
    # "application" must be a single application given as a string
    print(f"{bcolors.OKGREEN}Installing " + application + f"...{bcolors.ENDC}")
    subprocess.run(["rpm-ostree", "install", application], check=True)

def dl_persistent_service(url):
    print(f"{bcolors.OKGREEN}Downloading persistent Service file...{bcolors.ENDC}")
    request = urllib.request.Request(url, headers=hdr)
    writetofile(service_file, urllib.request.urlopen(request).read().decode('utf-8'))

def enable_service(service):
    print(f"{bcolors.OKGREEN}Enabling persistent Service file...{bcolors.ENDC}")
    subprocess.run(["systemctl", "enable", service], check=True)

def reboot():
    subprocess.run(["systemctl", "reboot"], check=True)

def finish_up(wait_time):
    wait_time_str = str(wait_time)
    print(f"{bcolors.OKGREEN}All done! Rebooting in " + wait_time_str + f" seconds...{bcolors.ENDC}")
    subprocess.run(["notify-send", "Dotfiles", "All done! Rebooting in " + wait_time_str + f" seconds..."])
    time.sleep(wait_time)

if getpass.getuser() == 'root':
    # Script logic
    rpmostree_install("ansible")
    dl_persistent_service(service_url)
    enable_service(service_name)
    finish_up(10)
    reboot()
else:
    print(f"{bcolors.WARNING}Please run the script with \"sudo\" in front{bcolors.ENDC}")