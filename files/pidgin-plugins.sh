#!/usr/bin/env sh
echo "Creating build Toolbox"
toolbox create -c build-purple-plugins
echo "Building purple-facebook"
toolbox run -c build-purple-plugins sudo dnf install -y gcc make glib glib-devel json-glib json-glib-devel libpurple libpurple-devel zlib zlib-devel
toolbox run -c build-purple-plugins git clone https://github.com/jgeboski/purple-facebook.git
echo 1
cd purple-facebook
echo 2
toolbox run -c build-purple-plugins sh autogen.sh
echo 3
toolbox run -c build-purple-plugins pwd
toolbox run -c build-purple-plugins make
echo "Installing purple-facebook"
toolbox run -c build-purple-plugins mkdir -p ~/.purple/plugins
echo 4
toolbox run -c build-purple-plugins cp pidgin/libpurple/protocols/facebook/.libs/libfacebook.so ~/.purple/plugins
# Cleanup
echo 5
rm -rf ~/purple-facebook
echo 6
toolbox rm --force build-purple-plugins 
echo end